//Christoph Ebler 
//MatNr 51411
//16.09.2015


typedef struct 
{
	//Dieser wird verwendet um den speicherplatz f�r name vorname,festnetzTel, mobilTel,EmailAdresse  des studenten und des Ansprchpartners zu speichern 
	//da er 2 mal ben�tigt wird hab ich den in einen weiteren typdef ausgelagert 
	char Name[20];
	char Vorname[20];
	char festTel[20];
	char mobilTel[20];
	char eMail[20];
} PERSON;

//Dieser wird verwendet um den speicherplatz f�r die Adresse des Studenten un der Firma  zu speichern 
	//da er 2 mal ben�tigt wird hab ich den in einen weiteren typdef ausgelagert 
typedef struct 
{
	char strasse[20];
	char nr[20];
	char plz[20];
	char ort[20];
}ADRESSE;



typedef struct 
{
//Student 
	char MatNr[20];
	PERSON persStudent;
	ADRESSE adrStudent ;
	char beginnDat[20];//hier wurde ein String verwendet da sonst f�hrende nullen abgeschnitten werden 

//PX Patner 
	char pxPartner [20];
	ADRESSE pxAdresse ;

	char pxPos [20];
	PERSON persAnsprech;
	

	bool RELEVANT ; // ist der datensatz �berhaubt relevant oder soll er als leer betrachtet werden


} STUDENT;

