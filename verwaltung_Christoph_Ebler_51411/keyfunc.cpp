//Christoph Ebler 
//MatNr 51411
//16.09.2015

#include<string.h>
#include <stdio.h>
#include<conio.h>
#include"fileIO.h"
#include"menuefunc.h"
#include <stdlib.h>
#include"error_Messages.h"

//Der funktion wird das Array mit den Studenten �bergeben 
// Diese Funktion sucht nach dem n�chsem freien feld in dem Array und giebt den index des feldes zur�ck
int find_Free_Space(STUDENT inStud[150])
{
	int nr = 0;
	int zaeler = 0;
	for (zaeler = 0;zaeler<150;zaeler++)
	{
		if (inStud[zaeler].RELEVANT == false)
		{
		nr = zaeler;
		break;
		}
	}
	return zaeler;
}

//Der funktion wird das Array mit den Studenten �bergeben 
// Die funktion �<berpr�<ft ob noch platz vorhanden ist und giebt das ergebnis zur�ck 
bool SpeicherplatzFrei(STUDENT inStud[150])
{
	bool flag = false;
	int zaeler = 0;
	for (zaeler = 0;zaeler<150;zaeler++)
	{
		if (inStud[zaeler].RELEVANT == false)
		{
		flag = true ;
		break;
		}
	}
	return flag;
}

//Der funktion wird das Array mit den Studenten und der dateiname der .txt datei  �bergeben 
//Die funktion Lie�t die datei ein.
//wenn das lesen Erfolgreich war Giebt sie true sonst false zur�ck.
bool initDB(char filename[20] ,STUDENT initStud[150])
{
	int wahl=0;
	bool retVal = false;
	if (readFile(filename,initStud)!=1)
	{
		printf("Das Oeffnen der Datenbasis war nicht erfolgreich \n\n");
		wahl = OpenDatFailMenue ();
		switch (wahl)
		{
		case 0:

		
			retVal = false;
			break;

		case 1:
		newfile (filename);
		printf("Neue Datenbasis angelegt \n");
		printf("Bitte dr�cken sie eine Beliebige taste um ins Haubtmen� zu gelangen \n");
		_getch();
		retVal = true;
		break;
		default :
			strZerror();
			break;
		
		}
	}
	else 
	{
	retVal = true;
	}
return retVal;
}

//Der funktion wird das Array mit den Studenten und der index des feldes �ebergeben
//die funktion l�scht alle daten aus dem feld des Arrays mit der nummer die �bergeben wurde
void initZeroOne(STUDENT iniStud[150],int i)
{
	//alle werte auf null setzen 
		//diese version ist mit sicherheit nicht die eleganteste aber mir sit so auf die schnelle nichts besseres eingefallen 
		//es funktioniert ja 
		char str[5] = " \0";
		strcpy( iniStud[i].MatNr,str);
		strcpy( iniStud[i].persStudent.Name,str);
		strcpy( iniStud[i].persStudent.Vorname,str);
		strcpy( iniStud[i].persStudent.mobilTel,str);
		strcpy( iniStud[i].persStudent.festTel,str);
		strcpy( iniStud[i].persStudent.eMail,str);
		strcpy( iniStud[i].beginnDat,str);
		strcpy( iniStud[i].adrStudent.strasse,str);
		strcpy( iniStud[i].adrStudent.nr,str);
		strcpy( iniStud[i].adrStudent.plz,str);
		strcpy( iniStud[i].adrStudent.ort,str);
		strcpy( iniStud[i].pxPartner,str);
		strcpy( iniStud[i].pxAdresse.strasse,str);
		strcpy( iniStud[i].pxAdresse.nr,str);
		strcpy( iniStud[i].pxAdresse.plz,str);
		strcpy( iniStud[i].pxAdresse.ort,str);
		strcpy( iniStud[i].persAnsprech.Name,str);
		strcpy( iniStud[i].persAnsprech.Vorname,str);
		strcpy( iniStud[i].persAnsprech.mobilTel,str);
		strcpy( iniStud[i].persAnsprech.festTel,str);
		strcpy( iniStud[i].persAnsprech.eMail,str);
		strcpy( iniStud[i].pxPos,str);
		iniStud[i].RELEVANT=false;
}

//er funktion wird das Array mit den Studenten �bergeben
//die funktion L�scht alle daten aus dem Array ;
void initZero(STUDENT iniStud[150])
{
	int i  =0;
	for (i=0;i<150;i++)
	{
		//alle werte auf null setzen 
		//diese version ist mit sicherheit nicht die eleganteste aber mir sit so auf die schnelle nichts besseres eingefallen 
		//es funktioniert ja 
		

		initZeroOne( iniStud,i);

	}
}

//Der funktion wird das Array mit den Studenten und der index des feldes �ebergeben
//die funktion l�scht alle daten aus dem feld des Arrays mit der nummer die �bergeben wurde
void datensatzLoeschen(STUDENT inStud[150],int Nr)
	{
		initZeroOne(inStud,Nr);
	}


//Der Funktion wird das Array mit den Studenten sowie der indes des zu bearbeiten Studenten �bergeben
//Sie  giebt den index des ausgew�hlten eintrages an das Hauptprogramm zur�ck!

int  BearbeitenMenue(STUDENT iniStud[150],int Nr)
	
	{
 int eingabe =0;
	printf("Welchen wert im datensatz  Nummer :%i Moechten sie Aendern \n",Nr);


	printf("Angaben Zum Studenten :\n");
	printf("\t1 = Matrikelnummer		=>	%s\n",iniStud[Nr].MatNr);
	printf("\t2 = Name des Studenten		=>	%s\n",iniStud[Nr].persStudent.Name);
	printf("\t3 = Vorname des Studenten	=>	%s\n",iniStud[Nr].persStudent.Vorname);
	printf("\t4 = Startdatum des Studiums	=>	%s\n",iniStud[Nr].beginnDat);
	printf("\t5 = Tel NR			=>	%s\n",iniStud[Nr].persStudent.festTel);
	printf("\t6 = Mobiel NR			=>	%s\n",iniStud[Nr].persStudent.mobilTel);
	printf("\t7 = Email Adrese		=>	%s\n",iniStud[Nr].persStudent.eMail);
	printf("\t8 = Strasse			=>	%s\n",iniStud[Nr].adrStudent.strasse);
	printf("\t9 = Haussnummer			=>	%s\n",iniStud[Nr].adrStudent.nr);
	printf("\t10 = Plz			=>	%s\n",iniStud[Nr].adrStudent.plz);
	printf("\t11 = Ort			=>	%s\n",iniStud[Nr].adrStudent.ort);
	

	
	printf("\nAngaben Zur Px Firma :\n");
	printf("\t12 = Name			=>	%s\n",iniStud[Nr].pxPartner);
	printf("\t13 = Strasse			=>	%s\n",iniStud[Nr].pxAdresse.strasse);
	printf("\t14 = Haussnummer		=>	%s\n",iniStud[Nr].pxAdresse.nr);
	printf("\t15 = Plz			=>	%s\n",iniStud[Nr].pxAdresse.plz);
	printf("\t16 = Ort			=>	%s\n",iniStud[Nr].pxAdresse.ort);

	printf("\n Angaben Zum Bereuer :\n");
	printf("\t17 = Name			=>	%s\n",iniStud[Nr].persAnsprech.Name);
	printf("\t18 = Vorname			=>	%s\n",iniStud[Nr].persAnsprech.Vorname);
	printf("\t19 = Position			=>	%s\n",iniStud[Nr].pxPos);
	printf("\t20 = Tel NR			=>	%s\n",iniStud[Nr].persAnsprech.festTel);
	printf("\t21 = Mobiel NR			=>	%s\n",iniStud[Nr].persAnsprech.mobilTel);
	printf("\t22 = Email Adrese		=>	%s\n\n\n",iniStud[Nr].persAnsprech.eMail);
	printf("\t0 = Beenden \n\n");
	printf("ihre Wahl ?:");
	fflush(stdin);
	scanf("%i",&eingabe);
	printf("\n");
	return(eingabe);

}

//der Funktion wird das Studenten Array sowie der index des studenten �bergeben der bearbeitet werden soll.
// Diese funktion enh�lt die auswertung der benutzereingabe und Schreibt die neu eingegebnen werte in die
//felder des ATDs des Studenten Arrays an der stelle die der funktion �bergeben wurde.
void datensatzBearbeiten(STUDENT inStud[150],int Nr)
	{
		int wahl =0;
		bool beenden =false;
		do 
		{
		myCls();
		char value[20];
		wahl = BearbeitenMenue(inStud,Nr);
		fflush(stdin);
		
		switch(wahl)
		{
			case 0:		
					
					beenden  = true;
					break;
			case 1:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].MatNr,value);
					
					break;
			case 2:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].persStudent.Name,value);
					
					break;
			case 3:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].persStudent.Vorname,value);
					
					break;
			case 4:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].beginnDat,value);
					
					break;
			case 5:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].persStudent.festTel,value);
					
					break;
			case 6:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].persStudent.mobilTel,value);
					
					break;
			case 7:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].persStudent.eMail,value);
					
					break;
			case 8:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].adrStudent.strasse,value);
					
					break;
			case 9:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].adrStudent.nr,value);
					
					break;
			case 10:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].adrStudent.plz,value);
					
					break;
			case 11:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].adrStudent.ort,value);
					
					break;
			case 12:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].pxPartner,value);
					
					break;
			case 13:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].pxAdresse.strasse,value);
					
					break;
			case 14:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].pxAdresse.nr,value);
					
					break;

			case 15:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].pxAdresse.plz,value);
					
					break;
			case 16:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].pxAdresse.ort,value);
					
					break;
			case 17:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].persAnsprech.Name,value);
					
					break;

			case 18:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].persAnsprech.Vorname,value);
					
					break;
			case 19:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].pxPos,value);
					
					break;
			case 20:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].persAnsprech.festTel,value);
					
					break;
			case 21:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].persAnsprech.mobilTel,value);
					
					break;
			case 22:		
					printf("Bitte den neuen wert Eingeben :");
					scanf("%s",value);
					strcpy( inStud[Nr].persAnsprech.eMail,value);
					
					break;

			default :
					strZerror();
					
					break;
					
		}

		}while(beenden ==false);

	}

//der Funktion wird das Studenten Array sowie der index des studenten �bergeben der ausgegeben werden soll.
// Diese Funktion giebt alle angaben des Studenten aus dessen index �bergeben wurde .
//die funktion giebt nichts zur�ck.
void show(int Nr,STUDENT iniStud[150])
	
	{
		if(iniStud[Nr].RELEVANT)
{
	printf("Ausgebe des datensatzes mit der Nummer :%i\n",Nr);


	printf("Angaben Zum Studenten :\n");
	printf("\tMatrikelnummer		=>	%s\n",iniStud[Nr].MatNr);
	printf("\tName des Studenten	=>	%s\n",iniStud[Nr].persStudent.Name);
	printf("\tVorname des Studenten	=>	%s\n",iniStud[Nr].persStudent.Vorname);
	printf("\tStartdatum des Studiums	=>	%s\n",iniStud[Nr].beginnDat);
	printf("\tTel NR			=>	%s\n",iniStud[Nr].persStudent.festTel);
	printf("\tMobiel NR		=>	%s\n",iniStud[Nr].persStudent.mobilTel);
	printf("\tEmail Adrese		=>	%s\n",iniStud[Nr].persStudent.eMail);
	printf("\tStrasse			=>	%s\n",iniStud[Nr].adrStudent.strasse);
	printf("\tHaussnummer		=>	%s\n",iniStud[Nr].adrStudent.nr);
	printf("\tPlz			=>	%s\n",iniStud[Nr].adrStudent.plz);
	printf("\tOrt			=>	%s\n",iniStud[Nr].adrStudent.ort);
	

	
	printf("\nAngaben Zur Px Firma :\n");
	printf("\tName			=>	%s\n",iniStud[Nr].pxPartner);
	printf("\tStrasse			=>	%s\n",iniStud[Nr].pxAdresse.strasse);
	printf("\tHaussnummer		=>	%s\n",iniStud[Nr].pxAdresse.nr);
	printf("\tPlz			=>	%s\n",iniStud[Nr].pxAdresse.plz);
	printf("\tOrt			=>	%s\n",iniStud[Nr].pxAdresse.ort);

	printf("\nAngaben Zum Bereuer :\n");
	printf("\tName			=>	%s\n",iniStud[Nr].persAnsprech.Name);
	printf("\tVorname			=>	%s\n",iniStud[Nr].persAnsprech.Vorname);
	printf("\tPosition		=>	%s\n",iniStud[Nr].pxPos);
	printf("\tTel NR			=>	%s\n",iniStud[Nr].persAnsprech.festTel);
	printf("\tMobiel NR		=>	%s\n",iniStud[Nr].persAnsprech.mobilTel);
	printf("\tEmail Adresse		=>	%s\n",iniStud[Nr].persAnsprech.eMail);
}
		else 
		{
			 myCls();
			DSnot_Exsiting_error();
			_getch();
		}
	
	// myCls();
}

//Diese funktion giebt alle studenten aus und erm�glicht das navigieren zwischen den studenten.
// ihr wird ein anzeigetext und das array mit den Studenten �bergeben.
bool ausgabeAlle (STUDENT iniStud[150],char text[20])
{
char wahl = '0';
int nr =0;
int index =0;
bool beenden =false;
	
    do
	{
		myCls();
	printf("%s \n\n\n", text);
	show( nr,iniStud);
	fflush(stdin);
	wahl =AusgabeMenue();
	switch (wahl)
	{
		//index out of range ex abfangen 
				case '0':		
					
					beenden  = true;
					break;
				case '4':		
					
						if (nr >0)
						{
							nr--;
						}
						else
						{
							nr=0;
						}
					
					
					
					break;
				case '6':
					
						if (nr <149)
						{
							nr++;
						}
						else
						{
							nr=149;
						}

					
				
					break;
				default :
					strZerror();
					break;
	}
	
	}while(beenden==false);
	
	return true ;
}

//Diese funktion giebt alle suchergbnisse aus und erm�glicht das navigieren zwischen den studenten.
//desweiteren kann sie �ber die flags Bearbeiten und l�schen konfiguriert werden sodass die option zum Bearbeiten oder l�schen
//direkt im men� angezeigt wird 
// ihr wird ein anzeigetext und das array mit den Studenten �bergeben. Sowie die flags zum bearbeiten oder l�schen 
bool ausgabeSuchergebnise (STUDENT iniStud[150],char text[20],int erg[150],int anz,bool bearbeiten,bool l�schen)
{
char wahl = '0';
int nr =erg[0];
int ergindex =0;
int index =0;
bool beenden =false;
	
    do
	{
		myCls();
	printf("Es Wurden %i Ergebnisse gefunden ! \n\n\n",anz);
	printf("%s \n\n\n", text);
	show( nr,iniStud);
	fflush(stdin);
	wahl =SuchergMenue(bearbeiten,l�schen );
	switch (wahl)
	{
		//index out of range ex abfangen 
				case '0':		
					
					beenden  = true;
					break;
				case '4':		
					if (ergindex >0)
						{
							ergindex--;
							nr =erg[ergindex];
						}
						else
						{
							nr =erg[0];
						}
					
					
					break;
				case '6':
					if (ergindex <(anz-1))
						{ergindex++;
							nr =erg[ergindex];
						}
						else
						{
							
							nr =erg[ergindex];
						}
				
					break;
				case '7':
					if (bearbeiten==true)
					{
					datensatzBearbeiten(iniStud,nr);
					}
				
					break;
				case '8':
					if (l�schen==true)
					{
					datensatzLoeschen(iniStud,nr);
					}
				
					break;
				default :
					strZerror();
					break;
	}
	
	}while(beenden==false);
	
	return true ;
}


// diese funktion Bekommt das studenten Array und ein Array zm speichern der ergebnisse �bergeben.
// die funktion Durchsucht das Array nach studenten mit der als suchwort eingegebenen MatNR
//sie schreibt die ergebnisse in das ergbnis Array 
int suchen_Nach_MatNr (STUDENT iniStud[150],int ergbnisse[150])
{
	char suchwort[20];
	int zaeler_stud =0;
	int zaeler_erg =0;
	printf("Bitte Geben sie die Matrikel nummer ein nach der gesucht werden soll:");
	
	scanf("%s",suchwort);
	
	for (zaeler_stud =0;zaeler_stud<150;zaeler_stud++)
	{
		if (strcmp(iniStud[zaeler_stud].MatNr, suchwort) == 0)
		{
			ergbnisse[zaeler_erg]=zaeler_stud;
			zaeler_erg++;
		}
	}
	printf("Es wurden %i ergebnise gefunden",zaeler_erg);
	

	return zaeler_erg;
	
}

// diese funktion Bekommt das studenten Array und ein Array zm speichern der ergebnisse �bergeben.
// die funktion Durchsucht das Array nach studenten mit der als suchwort eingegebenen Namen
//sie schreibt die ergebnisse in das ergbnis Array 
int  suchen_Nach_Name (STUDENT iniStud[150],int ergbnisse[150])
{
	char suchwort[20];
	int zaeler_stud =0;
	int zaeler_erg =0;
	printf("Bitte Geben sie den Namen ein nach der gesucht werden soll:");
	
	scanf("%s",suchwort);
	
	for (zaeler_stud =0;zaeler_stud<150;zaeler_stud++)
	{
		if (strcmp(iniStud[zaeler_stud].persStudent.Name, suchwort) == 0)
		{
			ergbnisse[zaeler_erg]=zaeler_stud;
			zaeler_erg++;
		}
	}
	printf("Es wurden %i ergebnise gefunden",zaeler_erg);
	return zaeler_erg;
}

// Diese Funktion Bekommt das studenten Array und ein Array zm speichern der ergebnisse �bergeben.
// die funktion Durchsucht das Array nach studenten mit der als suchwort eingegebenen Vornamen
//sie schreibt die ergebnisse in das ergbnis Array 
int suchen_Nach_VorName (STUDENT iniStud[150],int ergbnisse[150])
{
	char suchwort[20];
	int zaeler_stud =0;
	int zaeler_erg =0;
	printf("Bitte Geben sie die Matrikel nummer ein nach der gesucht werden soll:");
	
	scanf("%s",suchwort);
	
	for (zaeler_stud =0;zaeler_stud<150;zaeler_stud++)
	{
		if (strcmp(iniStud[zaeler_stud].persStudent.Vorname, suchwort) == 0)
		{
			ergbnisse[zaeler_erg]=zaeler_stud;
			zaeler_erg++;
		}
	}
	printf("Es wurden %i ergebnise gefunden",zaeler_erg);
	return zaeler_erg;
}

// Diese Funktion Bekommt das studenten Array und ein Array zm speichern der ergebnisse �bergeben.
// die funktion Durchsucht das Array nach studenten mit der als suchwort eingegebenen Firma 
//sie schreibt die ergebnisse in das ergbnis Array 
int  suchen_Nach_Firma (STUDENT iniStud[150],int ergbnisse[150])
{
	char suchwort[20];
	int zaeler_stud =0;
	int zaeler_erg =0;
	printf("Bitte Geben sie die Matrikel nummer ein nach der gesucht werden soll:");
	
	scanf("%s",suchwort);
	
	for (zaeler_stud =0;zaeler_stud<150;zaeler_stud++)
	{
		if (strcmp(iniStud[zaeler_stud].pxPartner, suchwort) == 0)
		{
			ergbnisse[zaeler_erg]=zaeler_stud;
			zaeler_erg++;
		}
	}
	printf("Es wurden %i ergebnise gefunden",zaeler_erg);
	return zaeler_erg;
}


//Diese Funktion Erm�glicht das durchsuchen der Datenbank nach  MatNR,Name ,Vorname, Firma 
//desweiteren kann sie �ber die flags Bearbeiten und l�schen konfiguriert werden sodass die option zum Bearbeiten oder l�schen
//direkt im men� angezeigt wird 
// ihr wird ein anzeigetext und das array mit den Studenten �bergeben.Sowie die flags zum bearbeiten oder l�schen  
void suchen(STUDENT inStud[150],char text[20],bool bearbeiten,bool l�schen)
{
	int ergbnisse[150];
	int anzahl = 0;
	char  wahl = ' ';
	bool sucheErfolgreich = false ;
	bool Beenden = false ;
	do 
	{	myCls();
		fflush(stdin);
		wahl = SuchKritMenue (text);
		sucheErfolgreich = false ;
		anzahl = 0;
		switch (wahl)
		{
				case '0':		
					Beenden = true;
					sucheErfolgreich = false ;
					
					break;

				case '1':		
				anzahl=suchen_Nach_MatNr(inStud,ergbnisse);
				sucheErfolgreich = true ;
					
					break;
				case '2':		
					anzahl=suchen_Nach_Name(inStud,ergbnisse);
					sucheErfolgreich = true ;
					
					break;
				case '3':		
					anzahl=suchen_Nach_VorName(inStud,ergbnisse);
					sucheErfolgreich = true ;
					
					break;

				case '4':		
					anzahl=suchen_Nach_Firma(inStud,ergbnisse);
					 sucheErfolgreich = true ;
					 
					break;
				

				default :
					sucheErfolgreich = false ;
					strZerror();
					break;
		};

		if (anzahl>0&&sucheErfolgreich == true)
		{
		 ausgabeSuchergebnise (inStud,text,ergbnisse,anzahl,bearbeiten, l�schen);
		}
		else
		{
			if (anzahl<=0&&Beenden == false )
			{
				KeinSuchergebnisError();
			}
		}
	}while(Beenden == false );
}

// diese Funktion dient zum Eingeben und speichern der daten in ein feld des Studenten Arrays 
// hief�r wird ihr das Array und der Dateiname f�<r die .txt datei �bergeben 
bool Eingabe (char filename[20],STUDENT inStud[150])
	
{  
	int Nr =0;
	bool beenden =false;
	bool saved =false;
	bool nochEinen = true ;
	char wahl ='0';
do{
	
	saved =false;
	
	if (SpeicherplatzFrei(inStud) ==true )
	{
	if (nochEinen ==true )
	{
	Nr=find_Free_Space(inStud);
	inStud[Nr].RELEVANT = true;
	fflush(stdin);
	myCls();
	printf("Eingab der Daten fuer den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie einen Matrikelnummer ein :" );
	scanf("%[^\n]s",inStud[Nr].MatNr);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie einen Namen fuer den Studenten ein :" );
	scanf("%[^\n]s",inStud[Nr].persStudent.Name);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie einen VorNamen fuer den Studenten ein :" );
	scanf("%[^\n]s",inStud[Nr].persStudent.Vorname);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie das Startdatum fuer den Studenten ein :" );
	scanf("%[^\n]s",inStud[Nr].beginnDat);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die FestnetzNr. des Studenten ein :" );
	scanf("%[^\n]s",inStud[Nr].persStudent.festTel);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die MobielNr. des Studenten ein :" );
	scanf("%[^\n]s",inStud[Nr].persStudent.mobilTel);

	fflush(stdin);

	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die Email Adresse. des Studenten ein :" );
	scanf("%[^\n]s",inStud[Nr].persStudent.eMail);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die Strasse fuer den Wohnort des Studenten ein :" );
	scanf("%[^\n]s",inStud[Nr].adrStudent.strasse);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die Haussnummer fuer den Wohnort des Studenten ein :" );
	scanf("%[^\n]s",inStud[Nr].adrStudent.nr);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die PLZ fuer den Wohnort des Studenten ein :" );
	scanf("%[^\n]s",inStud[Nr].adrStudent.plz);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie den Ortsnamen fuer den Wohnort des Studenten ein :" );
	scanf("%[^\n]s",inStud[Nr].adrStudent.ort);
	fflush(stdin);
	//Firma 
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie den Namen Des Praxispartners  ein :" );
	scanf("%[^\n]s",inStud[Nr].pxPartner);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die Strasse fuer die Adresse des Praxispartners ein :" );
	scanf("%[^\n]s",inStud[Nr].pxAdresse.strasse);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die Haussnummer fuer die Adresse des Praxispartners ein :" );
	scanf("%[^\n]s",inStud[Nr].pxAdresse.nr);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die PLZ fuer die Adresse des Praxispartners ein:" );
	scanf("%[^\n]s",inStud[Nr].pxAdresse.plz);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie den Ortsnamen fuer die Adresse des Praxispartners ein:" );
	scanf("%[^\n]s",inStud[Nr].pxAdresse.ort);
	fflush(stdin);
	//Betreuer
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie einen Namen fuer den Betreuer ein :" );
	scanf("%[^\n]s",inStud[Nr].persAnsprech.Name);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie einen VorNamen fuer den Betreuer ein :" );
	scanf("%[^\n]s",inStud[Nr].persAnsprech.Vorname);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die Position fuer den Betreuer ein :" );
	scanf("%[^\n]s",inStud[Nr].pxPos);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die FestnetzNr. des Betreuers ein :" );
	scanf("%[^\n]s",inStud[Nr].persAnsprech.festTel);
	fflush(stdin);
	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die MobielNr. des Betreuers ein :" );
	scanf("%[^\n]s",inStud[Nr].persAnsprech.mobilTel);
	fflush(stdin);
	

	myCls();
	printf("Eingab der Daten f�r den Datensatz Nr: %i \n\n",Nr );
	show( Nr,inStud);
	printf("\n\n");
	printf("Bitte geben sie die Email Adresse. des Betreuers ein :" );
	scanf("%[^\n]s",inStud[Nr].persAnsprech.eMail);


	myCls();
	show( Nr,inStud);
	}
	}
	else 
	{
	///Error speicher voll
		SpeicherVollError();
	}
	fflush(stdin);
	 wahl =EingabeMenue();
	 fflush(stdin);
	switch (wahl)
	{
		
				case '0':		
					
					beenden  = true;
					break;
				case '1':		
					 saved =true;
					 
					 writeFile(filename,inStud);
					 nochEinen =false;
					break;
				case '2':		
					
					nochEinen =true;
					
					break;

				default :
					strZerror();
					nochEinen =false;
					break;

	}
if (beenden ==true&&saved ==false&& nochEinen ==true)
{
	if (No_Save_error()==true&&saved ==false )
	{
	
	 writeFile(filename,inStud);
	 initDB( filename,inStud);
	}
	else 
	{
	 inStud[Nr].RELEVANT = false;
	 initZeroOne(inStud,Nr);
	}

}

}while(beenden==false);
	
	return true ;
}

//Diese Funktion erm�glicht das suchen und L�schen eiens Datensatzes aus der Datenbank
void loeschen(STUDENT inStud[150])
{
	suchen(inStud,"Loeschen",false,true);
}

//Diese Funktion erm�glicht das suchen und Bearbeiten eiens Datensatzes aus der Datenbank
void barbeiten(STUDENT inStud[150])
{
	suchen(inStud,"Bearbeiten",true,false);
}
